const cp = require("child_process");
const installer = require("./updater/installer.js");
const fs = require("fs");

let nmpid, nm;

console.log("Network Manager Launcher Started(" + process.pid + ")...");
if(nm != undefined)
{
    nm.on('error', function(err)
    {
        console.log("Network Manager Panel Error: ", err.toString());
    });

    nm.on('message', function(data)
    {
        console.log('Message Received From Network Manager Panel\nMessage:' + data.toString());
    });
}

process.stdin.on("data", function(data)
{
    let cmd = data.toString().toLowerCase().trim().split(" ");
    switch (cmd[0])
    {
        case 'start':
            if(fs.existsSync("./.env"))
            {
                startNMChild();
            }
            else
            {
                console.log("Network Manager Installation Does Not Exists!\nPlease Type `Install` To Run The Install Command");
            }
            break;
        case 'stop':
            console.log("Stopping Network Manager(" + nmpid + ")...");
            cp.exec("kill -9 " + nmpid);
            console.log("Stopped Network Manager");
            break;
            
        case 'kill':
            console.log("Stopping Network Manager(" + nmpid + ")...");
            cp.exec("kill -9 " + nmpid);
            console.log("Stopped Network Manager");
            console.log("Network Manager Launcher Stopping(" + process.pid + ")...");
            process.exit();
            break;
            
        case 'restart':
            console.log("Stopping Network Manager(" + nmpid + ")...");
            cp.exec("kill -9 " + nmpid);
            console.log("Stopped Network Manager");
            startNMChild();
            break;
        case 'install':
            if(!fs.existsSync("./.env"))
            {
				installer.install()
            }
            else
            {
                console.log("Network Manager Installation Exists!\nPlease Type `Update` To Update Network Manager or Type `Uninstall` To Remove Network Manager Command");
            }
            break;
        case 'help':
            console.log("Network Manager Command Help");
			break;
        case 'update':
			break;
        case 'upgrade':
			break;			
        case 'uninstall':
			break;
    }
    
});

function startNMChild()
{
	let autoupdater = require("./updater/auto-updater.js");
    let versionResult = autoupdater.versionCheck();
    if(versionResult == 1)
    {
        console.log("Update Avilable");
    }
    else if(versionResult == 0)
    {
        console.log("You Are Up To Date");
    }
    else if(versionResult == -1)
    {
        console.log("Your Version is ahead of the released updates");
    }

    nm = cp.fork("./bin/www");
    nmpid = nm.pid;
    console.log("Starting Network Manager(" + nmpid + ")...");
}