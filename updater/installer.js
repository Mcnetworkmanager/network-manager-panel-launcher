const fs = require('fs');
const request = require("request");
const admzip = require('adm-zip');
const progress = require('progress-stream');

function installNM()
{
    var dlProgress = progress({time: 1000});

    dlProgress.on('progress', function(progress) {
        //console.log("Progress: " + Math.round(progress.percentage) + "% / 100%");
    });

    request("https://www.mcnetworkmanager.com/api/version.json", function (error, response, body)
    {
        if (!error && response.statusCode == 200)
        {
            versionDataJson = JSON.parse(body);            

            let fullURL = versionDataJson[0].downloadURL;
            let zipFile = fullURL.split("/").pop(1);
            let tempDownloads = "./";
            let downloadedZipPath = tempDownloads + zipFile;
            let extractionZipPath = "./";

            if(fs.existsSync("./" + zipFile))
            {
                fs.unlinkSync("./" + zipFile);
            }
            
            console.log("Starting To Download Network Manager Panel Version: " + versionDataJson[0].downloadVersion + "\n");
			
            request.get(fullURL).on('error', function(error)
            {
                console.error(error);
                
            }).pipe(dlProgress).pipe(fs.createWriteStream(downloadedZipPath)).on('finish', function()
            {
				console.log("Finished Downloading Network Manager Panel\n");
                try
                {
					console.log("Starting To Extract Network Manager Panel Version: " + versionDataJson[0].downloadVersion + "\n");
                    var zip = new admzip(downloadedZipPath);
                    zip.extractAllTo(extractionZipPath, false);
					console.log("Finished Extracting Network Manager Panel\n");
					console.log("Deleting " + zipFile + "\n");
                    fs.unlinkSync(downloadedZipPath);
					console.log("Deleted " + zipFile + " Successfully\n");
					console.log("Network Manager Panel Installed Successfully")
					console.log("Please Restart Network Manager Launcher");
					process.exit();
                    
                }
                catch (exception)
                {
                    console.error(exception);
                    
                }
            });
        }
        else
        {
            console.log("Error Getting Dowload Data!");
            
        }
    });
}

module.exports.install = installNM

