function downloadStart()
{
    console.log('File Download Started');
}

function downloadProgress()
{
    console.log('File Download In Progress');
}

function downloadFinished()
{
    console.log('File Download Finished');
}

function downloadFailed()
{
    console.log('File Download Failed');
}

module.exports.downloadStart = downloadStart;
module.exports.downloadProgress = downloadProgress;
module.exports.downloadFinished = downloadFinished;
module.exports.downloadFailed = downloadFailed;