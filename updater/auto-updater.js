 const fs = require('fs');
const compareVersion = require('compare-versions');
const panelSettings = require('../bin/panel.js');
const request = require("request");
const downloadManager = require('./downloadEvent');

function versionCheck()
{
    let remoteVersion;
    request("https://www.mcnetworkmanager.com/api/version.json", function (error, response, body)
    {
        if (!error && response.statusCode == 200)
        {
            versionDataJson = JSON.parse(body);
            remoteVersion = versionDataJson[0].downloadVersion;
            console.log(remoteVersion);
            
            let localVersion = panelSettings.getVersion();
            console.log(localVersion + " --> " + remoteVersion);
            let versionResult = compareVersion(remoteVersion.toString(), localVersion.toString());
            if(versionResult == 1)
            {
                console.log("Update Avilable");
                return [1, remoteVersion];
            }
            else if(versionResult == 0)
            {
                console.log("You Are Up To Date");
                return [0, remoteVersion];
            }
            else if(versionResult == -1)
            {
                console.log("Your Version is ahead of the released updates");
                return [-1, remoteVersion];
            }
            else
            {
                console.log("Error Occured When Checking Version Numbers");
                return [-2, ""];
            }
        }
    });
}

function downloadUpdate()
{

}

function extractUpdate()
{

}

function restartApp()
{

}

function autoUpdate()
{

}

function verifyUpdate()
{

}

function updateNM()
{

}

module.exports.versionCheck = versionCheck;
module.exports.update = updateNM;

